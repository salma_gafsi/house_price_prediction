# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 22:41:58 2020

@author: Salma
"""

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
import pickle



df = pd.read_csv(r'C:\Users\Salma\Desktop\Test-PFE\Data1.csv')

#print(df.head())
#print(df.describe())
#print(df.shape)

#######################Data Preprocessing#######################

#supprimer les lignes contenant une valeur null
df = df.dropna()
print(df.shape) # ----> par des lignes vides


plt.figure(figsize=(10, 8))
sns.distplot(df['prix'])
plt.show()

plt.figure(figsize=(10, 8))
sns.distplot(df['prix'].apply(np.log))
plt.show()

plt.figure(figsize=(16,16))
sns.heatmap(df.corr())
plt.show()




plt.subplots(figsize=(12,9))
sns.distplot(df['prix'], fit=stats.norm)

# Get the fitted parameters used by the function

(mu, sigma) = stats.norm.fit(df['prix'])



#Check again for more normal distribution

plt.subplots(figsize=(12,9))
sns.distplot(df['prix'], fit=stats.norm)

# Get the fitted parameters used by the function

(mu, sigma) = stats.norm.fit(df['prix'])

# plot with the distribution

plt.legend(['Normal dist. ($\mu=$ {:.2f} and $\sigma=$ {:.2f} )'.format(mu, sigma)], loc='best')
plt.ylabel('Frequency')

#Probablity plot

fig = plt.figure()
stats.probplot(df['prix'], plot=plt)
plt.show()

#plot of missing value attributes

plt.figure(figsize=(12, 6))
sns.heatmap(df.isnull())
plt.show()



train_corr = df.select_dtypes(include=[np.number])
print(train_corr.shape)
del train_corr['id_mutation']

corr = train_corr.corr()
plt.subplots(figsize=(20,9))
sns.heatmap(corr, annot=True)


col = ['prix', 'surface_reelle_bati', 'nombre_pieces_principales', 'longitude', 'latitude', 'distance_mer']
sns.set(style='ticks')
sns.pairplot(df[col], size=3, kind='reg')

print("Find most important features relative to target")
corr = df.corr()
corr.sort_values(['prix'], ascending=False, inplace=True)
print(corr.prix)




from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn import ensemble

Y = df['prix']                #value for prediction
X = df.drop(['prix', 'date_mutation','id_mutation'], axis = 1) #Data
#X = df.drop('date_mutation', axis = 1) #Effacer date_mutation car elle n'a pas d'effet sur le prix(tous les maisons ont la même date)


# Split data into train and test formate
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=80)
print(X_test)
#print(Y_train)



"""
#### Linear Regression
from sklearn.linear_model import LinearRegression
regressior=LinearRegression()
regressior.fit(X_train, Y_train)
print("Accuracy --> ", regressior.score(X_test, Y_test)*100)
"""

#### RandomForestRegressor
from sklearn.ensemble import RandomForestRegressor
model = RandomForestRegressor(n_estimators=900)

#Fit
model.fit(X_train, Y_train)
print("Accuracy --> ", model.score(X_test, Y_test)*100)

Ypred = model.predict(X_test)
print(Ypred[0])

"""
#### GradientBoostingRegressor
from sklearn.ensemble import GradientBoostingRegressor
GBR = GradientBoostingRegressor(loss='ls', learning_rate=0.05, n_estimators=250, max_depth=6)

GBR.fit(X_train, Y_train)
print("Accuracy --> ", GBR.score(X_test, Y_test)*100)
"""


#dump model regressior on the disk
pickle.dump(model,open('model.pkl','wb'))

#load model from the disk
model=pickle.load(open('model.pkl','rb'))


